#!/bin/sh
#if [ "$(echo "$TERM" | grep "kitty" )" ]; then
#    IMG_CMD="kitty +kitten icat"
#IMG_CMD="jp2a"
#    case "$1" in
#            *.tar*) tar tf "$1";;
#            *.zip) unzip -l "$1";;
#            *.rar) unrar l "$1";;
#            *.7z) 7z l "$1";;
#            *.jpg | *.JPG | *.jpeg) $IMG_CMD "$1";;
#            *.png | *.svg | *.gif) $IMG_CMD "$1";;
#            *.mp4 | *.mkv | *.webm | *.avi | *.mpg | *.mpeg | *.ogv | *.flv | *.wmv | *.WMV) ffmpegthumbnailer -i "$1" -c jpeg -o - | $IMG_CMD -;; 
#            #*.jpg | *.JPG | *.jpeg) jp2a "$1" -b --width=$(($(tput cols)/2 - 10));;
#            #*.png | *.svg | *.gif) convert "$1" jpg:- | jp2a - -b --width=$(($(tput cols)/2 - 10));;
#            #*.mp4 | *.mkv | *.webm | *.avi | *.mpg | *.mpeg | *.ogv | *.flv | *.wmv | *.WMV) ffmpegthumbnailer -i "$1" -c jpeg -o - | jp2a - -b --width=$(($(tput cols)/2 - 10));; 
#            *.pdf) pdftotext "$1" -;;
#            *) highlight -O ansi "$1" || cat "$1";;
#    esac

#else
    case "$1" in
            *.tar*) tar tf "$1";;
            *.zip) unzip -l "$1";;
            *.rar) unrar l "$1";;
            *.7z) 7z l "$1";;
            *.jpg | *.JPG | *.jpeg) viu -t "$1" -w $(($(tput cols)/2 - 10));;
            *.png | *.svg | *.gif) viu -t "$1" -w $(($(tput cols)/2 - 10)) ;;
            *.mp4 | *.mkv | *.webm | *.avi | *.mpg | *.mpeg | *.ogv | *.flv | *.wmv | *.WMV) ffmpegthumbnailer -i "$1" -c jpeg -o - | viu -t -;; 
            #*.jpg | *.JPG | *.jpeg) jp2a "$1" -b --width=$(($(tput cols)/2 - 10));;
            #*.png | *.svg | *.gif) convert "$1" jpg:- | jp2a - -b --width=$(($(tput cols)/2 - 10));;
            #*.mp4 | *.mkv | *.webm | *.avi | *.mpg | *.mpeg | *.ogv | *.flv | *.wmv | *.WMV) ffmpegthumbnailer -i "$1" -c jpeg -o - | jp2a - -b --width=$(($(tput cols)/2 - 10));; 
            *.pdf) pdftotext "$1" -;;
            *) highlight -O ansi "$1" || cat "$1";;
    esac

#fi

