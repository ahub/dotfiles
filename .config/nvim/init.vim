if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
  silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif


"let g:ale_completion_enabled = 1

call plug#begin('~/.local/share/nvim/plugged')
Plug 'itchyny/lightline.vim'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'preservim/nerdtree'
"Plug 'morhetz/gruvbox'
Plug 'dense-analysis/ale'

Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }

" (Optional) Multi-entry selection UI.
Plug 'junegunn/fzf'

 "deoplete - code completion. REQUIRES: :echo has("python3")
  if has('nvim')
    Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
  else
    Plug 'Shougo/deoplete.nvim'
    Plug 'roxma/nvim-yarp'
    Plug 'roxma/vim-hug-neovim-rpc'
  endif
  " deoplete-clang - code completion for C/C++
  Plug 'zchee/deoplete-clang'
  


call plug#end()

"enable deoplete
let g:deoplete#enable_at_startup = 1

"lightline config
set noshowmode
let g:lightline = {
      \ 'colorscheme': 'darcula',
      \ } 

"syntax numbers, wildmode
    syntax on
    set ruler
    set number "relativenumber
    set wildmode=longest,list,full

"Tab Config
    set expandtab   "converts tabs to spaces
    set shiftwidth=4
    set tabstop=4

"open nerd tree
map <C-n> :NERDTreeToggle<CR>

"split navigation
nnoremap  <silent>   <tab>  :if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bnext<CR>
nnoremap  <silent> <s-tab>  :if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bprevious<CR>


"make vim use system clipboard by default. ONLY FOR NEOVIM
set clipboard+=unnamedplus

let vim_markdown_preview_browser='surf'
let vim_markdown_preview_use_xdg_open=1

"enable unicode support if possible
if has("multi_byte")
  if &termencoding == ""
    let &termencoding = &encoding
  endif
  set encoding=utf-8
  setglobal fileencoding=utf-8
  " Uncomment to have 'bomb' on by default for new files.
  " Note, this will not apply to the first, empty buffer created at Vim startup.
  "setglobal bomb
  set fileencodings=ucs-bom,utf-8,latin1
endif

