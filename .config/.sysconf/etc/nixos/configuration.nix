# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
#

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
     # ./sway-configuration.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "rias"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp5s0.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
   i18n = {
     consoleFont = "Lat2-Terminus16";
     consoleKeyMap = "us";
     defaultLocale = "en_US.UTF-8";
   };

  # Set your time zone.
  time.timeZone = "America/Chicago";

  # List packages installed in system profile. To search, run:
  # $ nix search wget

   nixpkgs.config.allowUnfree = true;

   hardware.opengl.driSupport32Bit = true;
   hardware.opengl.extraPackages32 = with pkgs.pkgsi686Linux; [ libva ];
   hardware.pulseaudio.support32Bit = true;


   fileSystems."/home/alex/HDDA" =
    { device = "/dev/disk/by-uuid/b7d683d4-9337-433b-8d33-1035ef7dbcdf";
      fsType = "ext4";
    };

   fileSystems."/media/Seagate_HDD/" =
    { device = "/dev/disk/by-uuid/3ec3e778-2b12-469d-92be-95226c0cf759";
      fsType = "ext4";
    };

   services.flatpak.enable = true;

   environment.systemPackages = with pkgs; [
     #System programs
     wget file unzip unrar zsh 

     #KDE
    # redshift-plasma-applet redshift     
     #GNOME
     gnome3.gnome-tweaks

     #Graphics
     vulkan-loader
     

     #CLI programs
     vim htop radeontop 
     
     #GUI programs
     firefox palemoon transmission-gtk mcomix
    
     #Theming
     gnome3.adwaita-icon-theme gnome3.defaultIconTheme

     #Gaming
     wineFull lutris steam

     #Development
     git go lua love_11
     mono dotnetPackages.MonoAddins
     gcc python gnumake 

  #needed to build wc-gammactl (maybe not all of this)
   #  meson ninja clang llvm
   #  libgcc
   #  gcc-unwrapped gnome3.glade gtk3 gtkmm3  cmake pkg-config wlroots
   ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  # List services that you want to enable:
  
  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  networking.firewall.enable = false;

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # Enable the X11 windowing system.
#  services.xserver.enable = true;
#  services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable touchpad support.
  # services.xserver.libinput.enable = true;

  # Enable the KDE Desktop Environment.
  #services.xserver.displayManager.sddm.enable = true;
  #services.xserver.desktopManager.plasma5.enable = true;

  # Enable GNOME Desktop Environment
services.xserver.enable = true;
services.xserver.displayManager.gdm.enable = true;
services.xserver.desktopManager.gnome3.enable = true;
#services.dbus.packages = with pkgs; [ gnome2.GConf ];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.alex = {
     isNormalUser = true;
    # shell = pkgs.zsh;
     extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
   };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.09"; # Did you read the comment?

}

