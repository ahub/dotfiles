# PLEASE READ THE MAN PAGE BEFORE EDITING THIS FILE!
# https://htmlpreview.github.io/?https://github.com/conformal/spectrwm/blob/master/spectrwm.html
# NOTE: all rgb color values in this file are in hex! see XQueryColor for examples

workspace_limit	        = 9 
focus_mode	        	= follow 
focus_close	        	= last 
focus_close_wrap    	= 1 
focus_default		    = first 
spawn_position	     	= next 
workspace_clamp	        = 0 

warp_pointer		    = 1

# Window Decoration
border_width		    = 1
#color_focus		        = rgb:bb/c5/ff
#color_focus_maximized   = yellow
#color_unfocus		    = rgb:88/88/88
#color_unfocus_maximized	= rgb:88/88/00
region_padding      	= 6 
tile_gap		        = 6

#Colors
color_focus		        = rgb:bb/c5/ff
color_focus_maximized   = yellow
color_unfocus		    = rgb:88/88/88
color_unfocus_maximized	= rgb:88/88/00

bar_border[1]	    	= rgb:28/2a/36
bar_border_unfocus[1]	= rgb:28/2a/36
bar_color[1]	    	= rgb:28/2a/36, rgb:00/80/80
bar_color_selected[1]	= rgb:00/80/80
bar_font_color[1]   	= rgb:bb/c5/ff, rgb:e1/ac/ff, rgb:dd/ff/a7, rgb:ff/8b/92, rgb:ff/e5/85, rgb:89/dd/ff
bar_font_color_selected	= black


# Bar Settings
bar_action              = /home/alex/.local/scripts/spectrwm-bar.sh
bar_action_expand       = 1
bar_enabled	        	= 1
bar_border_width	    = 1
#bar_border[1]	    	= rgb:28/2a/36
#bar_border_unfocus[1]	= rgb:28/2a/36
#bar_color[1]	    	= rgb:28/2a/36, rgb:00/80/80
#bar_color_selected[1]	= rgb:00/80/80
bar_delay               = 5
#bar_font_color[1]   	= rgb:bb/c5/ff, rgb:e1/ac/ff, rgb:dd/ff/a7, rgb:ff/8b/92, rgb:ff/e5/85, rgb:89/dd/ff
#bar_font_color_selected	= black
bar_font       = mono:size=10, symbola:size=10
bar_justify    = center 
bar_format	        	= +|L+1<+N:+I +S +F +W +|R+A+1<+@fg=5; %a %b %d [%R]

workspace_indicator 	= listcurrent,listactive,markcurrent,printnames
bar_at_bottom	     	= 0
stack_enabled	    	= 1
clock_enabled	    	= 1
clock_format	    	= %a %b %d %R %Z %Y
iconic_enabled      	= 0
maximize_hide_bar	    = 0
window_class_enabled	= 1
window_instance_enabled	= 1
window_name_enabled 	= 1
verbose_layout	    	= 1
urgent_enabled	    	= 1
urgent_collapse         = 0


# PROGRAMS

# Validated default programs:
program[lock] = slock
program[term] = alacritty
program[menu] = dmenu_run -nb $bar_color -nf $bar_font_color -sb $bar_border -sf $bar_color_selected -fn "mono:size=9"
program[search] = dmenu -nb $bar_color -nf $bar_font_color -sb $bar_border  -sf $bar_color_selected -fn "mono:size=9" 
# program[name_workspace]	= dmenu $dmenu_bottom -p Workspace -fn $bar_font -nb $bar_color -nf $bar_font_color -sb $bar_color_selected -sf $bar_font_color_selected

# To disable validation of the above, free the respective binding(s):
# bind[]		= MOD+Shift+Delete	# disable lock
# bind[]		= MOD+Shift+Return	# disable term
# bind[]		= MOD+p			# disable menu

# Optional default programs that will only be validated if you override:
# program[screenshot_all]	= screenshot.sh full	# optional
# program[screenshot_wind]	= screenshot.sh window	# optional
# program[initscr]	= initscreen.sh			# optional

# EXAMPLE: Define 'firefox' action and bind to key.
# program[firefox]	= firefox http://spectrwm.org/
# bind[firefox]		= MOD+Shift+b

# QUIRKS
# Default quirks, remove with: quirk[class:name] = NONE
# quirk[MPlayer:xv]			= FLOAT + FULLSCREEN + FOCUSPREV
# quirk[OpenOffice.org 2.4:VCLSalFrame]	= FLOAT
# quirk[OpenOffice.org 3.0:VCLSalFrame]	= FLOAT
# quirk[OpenOffice.org 3.1:VCLSalFrame]	= FLOAT
# quirk[Firefox-bin:firefox-bin]		= TRANSSZ
# quirk[Firefox:Dialog]			= FLOAT
# quirk[Gimp:gimp]			= FLOAT + ANYWHERE
# quirk[XTerm:xterm]			= XTERM_FONTADJ
# quirk[xine:Xine Window]			= FLOAT + ANYWHERE
# quirk[Xitk:Xitk Combo]			= FLOAT + ANYWHERE
# quirk[xine:xine Panel]			= FLOAT + ANYWHERE
# quirk[Xitk:Xine Window]			= FLOAT + ANYWHERE
# quirk[xine:xine Video Fullscreen Window] = FULLSCREEN + FLOAT
# quirk[pcb:pcb]				= FLOAT



# Region containment
# Distance window must be dragged/resized beyond the region edge before it is
# allowed outside the region.
# boundary_width 		= 50

# Remove window border when bar is disabled and there is only one window in workspace
# disable_border		= 1

# Dialog box size ratio when using TRANSSZ quirk; 0.3 < dialog_ratio <= 1.0
# dialog_ratio		= 0.6

# Split a non-RandR dual head setup into one region per monitor
# (non-standard driver-based multihead is not seen by spectrwm)
# region		= screen[1]:1280x1024+0+0
# region		= screen[1]:1280x1024+1280+0

# Launch applications in a workspace of choice
# autorun		= ws[1]:xterm
# autorun		= ws[2]:xombrero http://www.openbsd.org

autorun = ws[1]:sxhkd
autorun = ws[1]:dunst
autorun = ws[1]:picom
autorun = ws[1]:redshift
autorun = ws[1]:mpd
autorun = ws[1]:wp-load.sh
autorun = ws[1]:devmon
autorun = ws[1]:xautolock -time 30 -locker slock

autorun = ws[1]:pipewire
autorun = ws[1]:pipewire-pulse
autorun = ws[1]:pipewire-media-session


# Customize workspace layout at start
# layout		= ws[1]:4:0:0:0:vertical
# layout		= ws[2]:0:0:0:0:horizontal
# layout		= ws[3]:0:0:0:0:fullscreen
# layout		= ws[4]:4:0:0:0:vertical_flip
# layout		= ws[5]:0:0:0:0:horizontal_flip

# Set workspace name at start
# name			= ws[1]:IRC
# name			= ws[2]:Email
# name			= ws[3]:Browse
# name			= ws[10]:Music

# Mod key, (Windows key is Mod4) (Apple key on OSX is Mod2)
modkey = Mod1

# This allows you to include pre-defined key bindings for your keyboard layout.
keyboard_mapping = ~/.config/spectrwm/spectrwm_us.conf





