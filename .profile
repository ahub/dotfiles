#!/usr/bin/env sh

#run my startup script to run background services

# Get the aliases and functions
#[ -f $HOME/.bashrc ] && . $HOME/.bashrc


export SXHKD_SHELL='/bin/sh'

export PF_ASCII="artix"  #set ascii art for pfetch

export XCURSOR_THEME=Adwaita

#remove things from $HOME
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CONFIG_DIRS="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DOCUMENTS_DIR="$HOME/docs"
export XDG_DOWNLOAD_DIR="$HOME/dl"
export XDG_MUSIC_DIR="$HOME/media/audio/music"
export XDG_PICTURES_DIR="$HOME/media/img"
export XDG_VIDEOS_DIR="$HOME/media/video"
export HISTFILE="$XDG_DATA_HOME"/bash/history
#XDG_DESKTOP_DIR="$HOME/Desktop"
#XDG_PUBLICSHARE_DIR="$HOME/Public"
#XDG_TEMPLATES_DIR="$HOME/Templates"
#export XDG_RUNTIME_DIR="/run/"

#export XAUTHORITY="$XDG_RUNTIME_DIR/Xauthority"
export ZDOTDIR="$HOME/.config/zsh"
export NOTMUCH_CONFIG="${XDG_CONFIG_HOME:-$HOME/.config}/notmuch-config"
export GTK2_RC_FILES="${XDG_CONFIG_HOME:-$HOME/.config}/gtk-2.0/gtkrc-2.0"
export WGETRC="${XDG_CONFIG_HOME:-$HOME/.config}/wget/wgetrc"
export INPUTRC="${XDG_CONFIG_HOME:-$HOME/.config}/inputrc"
#export ALSA_CONFIG_PATH="$XDG_CONFIG_HOME/alsa/asoundrc"
export WINEPREFIX="$XDG_DATA_HOME/wineprefixes/default"
export STACK_ROOT="$XDG_DATA_HOME"/stack
export PASSWORD_STORE_DIR="${XDG_DATA_HOME:-$HOME/.local/share}/password-store"
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export CARGO_HOME="${XDG_DATA_HOME:-$HOME/.local/share}/cargo"
export GOPATH="${XDG_DATA_HOME:-$HOME/.local/share}/go"
export ANSIBLE_CONFIG="${XDG_CONFIG_HOME:-$HOME/.config}/ansible/ansible.cfg"
export GDBHISTFILE="$XDG_DATA_HOME"/gdb/history

#other settings
export QT_QPA_PLATFORMTHEME="gtk2"  # Have QT use gtk2 theme. must have qt5-styleplugins installed
#export QT_QPA_PLATFORMTHEME="qt5ct"  #have QT use qt5ct theme  

export LESSHISTFILE=/dev/null

#get colored man pages
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

export MANPAGER=less

#set preferred programs
export EDITOR=vim
export VISUAL=vim
export BROWSER=browser.sh
export TERMINAL=st

#sfeed vars
export SFEED_PLUMBER="$BROWSER"
export SFEED_URL_FILE="$HOME/.local/share/sfeed/read"

#lf icons
source $HOME/.config/lf/lf-icons.sh


# Add ~/.local/bin/ ~/.local/scripts/ and to PATH
    export PATH="$HOME/.local/bin:$PATH"
    export PATH="$HOME/.local/appimages:$PATH"
    export PATH="$HOME/.local/scripts:$PATH"
    #export PATH=$GOPATH/bin:$PATH

export MANPATH="$MANPATH:/usr/local/man/"

#bspwm config
PANEL_FIFO=/tmp/panel-fifo
PANEL_HEIGHT=24
PANEL_FONT="-*-fixed-*-*-*-*-10-*-*-*-*-*-*-*"
PANEL_WM_NAME=bspwm_panel
export PANEL_FIFO PANEL_HEIGHT PANEL_FONT PANEL_WM_NAME

#fix arduino ide issues
export AWT_TOOLKIT=MToolkit


#[[ $XDG_VTNR -le 2 ]] && [ -f ~/.xinitrc ]  && startx 
#[ "$(tty)" = "/dev/tty1" ] && [ -f ~/.xinitrc ] && startx
if [ -z "$DISPLAY" ] && [ "$(tty)" = /dev/tty1 ]; then
    sx
    #dwl -s "$HOME"/dwl-startup.sh 2> /dev/null > "$HOME"/.cache/dwltags
    #exec dbus-launch --exit-with-session river
    #startx
fi
