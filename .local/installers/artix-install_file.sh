#!/usr/bin/env sh

! [ -f "$1" ] && echo "input file list to install packages from." && exit

pkgm_install() {

    sudo pacman -Syu
#add arch repo

    if [ -z "$(grep "^\[lib32" /etc/pacman.conf)" ]; then

        echo "lib32 not enabled. Appending lines to /etc/pacman.conf"
        sleep 1

        sudo sh -c 'echo "[lib32]" >> /etc/pacman.conf'
        sudo sh -c 'echo "Include = /etc/pacman.d/mirrorlist" >> /etc/pacman.conf'
        sudo sh -c 'echo "" >> /etc/pacman.conf'


        #TODO lib32 is not found on fresh install, need to modify pacman.conf
        sudo pacman -S --noconfirm artix-archlinux-support lib32-artix-archlinux-support

        sudo pacman-key --populate archlinux

    fi





    if [ -z "$(grep "community" /etc/pacman.conf)" ]; then
        
            sudo sh -c 'echo "[extra]" >> /etc/pacman.conf'
            sudo sh -c 'echo "Include = /etc/pacman.d/mirrorlist-arch" >> /etc/pacman.conf'
            sudo sh -c 'echo "" >> /etc/pacman.conf'
            sudo sh -c 'echo "[community]" >> /etc/pacman.conf'
            sudo sh -c 'echo "Include = /etc/pacman.d/mirrorlist-arch" >> /etc/pacman.conf'
            sudo sh -c  'echo "" >> /etc/pacman.conf'
            sudo sh -c 'echo "[multilib]" >> /etc/pacman.conf'
            sudo sh -c 'echo "Include = /etc/pacman.d/mirrorlist-arch" >> /etc/pacman.conf'
        

#add universe repo
    #{ 
        sudo sh -c 'echo "[universe]" >> /etc/pacman.conf'
        sudo sh -c 'echo "Server = https://universe.artixlinux.org/\$arch" >> /etc/pacman.conf'
        sudo sh -c 'echo "Server = https://mirror.pascalpuffke.de/artix-universe/\$arch" >> /etc/pacman.conf'
    #} >> /etc/pacman.conf

    fi


	echo "I AM HERE"
    #install all packages from file
    sudo sh -c "cat $1 | xargs pacman -S --needed"
	echo "END OF INSTALL CMD"

#link services
    #sudo ln -s /etc/runit/sv/tlp /run/runit/service/
    #sudo ln -s /etc/runit/sv/bluetoothd /run/runit/service/
    sudo ln -s /etc/runit/sv/ufw /run/runit/service/
    sudo ln -s /etc/runit/sv/cronie /run/runit/service/
    sudo ln -s /etc/runit/sv/chrony /run/runit/service/



    input=""
    while [ "$input" != "DONE" ];
    do
        ls -l /etc/runit/sv/

        echo "enter name of service to enable. enter DONE when finished"
        read -r input

        if [ "$input" = "DONE" ]; then
            echo "finished setting up services."
        else 
            sudo ln -s /etc/runit/sv/"$input" /run/runit/service/
        fi
    done


    #sudo ln -s /etc/runit/sv/iwd /run/runit/service/

    #setup ly display manager TODO get this working
    #yay -S ly
    #echo "#!/bin/sh" > ~/.xprofile
    #echo ". ~/.local/inits/startup.sh" >> ~/.xprofile
    #echo "startup" >> ~/.xprofile

    echo "install and enable lightdm?(y/N)"
    read -r input
    if [ "$input" = "y" ] || [ "$input" = "Y" ]; then
        sudo pacman -S --noconfirm lightdm lightdm-runit lightdm-gtk-greeter lightdm-gtk-greeter-settings
        sudo ln -s /etc/runit/sv/lightdm /run/runit/service/
        sudo cp -r ~/.local/share/themes/Nordic-standard-buttons /usr/share/themes
        sudo cp -r ~/.local/share/icons/Papirus-Dark /usr/share/icons/
    fi

    echo "install nvidia proprietary drivers?(y/N)"
    read -r input
    if [ "$input" = "y" ] || [ "$input" = "Y" ]; then
        sudo mkdir -p /etc/pacman.d/hooks/
        sudo cp ~/.config/.sysconf/etc/pacman.d/hooks/nvidia.hook /etc/pacman.d/hooks/
        sudo pacman -S --noconfirm   nvidia nvidia-settings nvtop
    fi


    echo "install virt-manager?(y/N)"
    read -r input
    if [ "$input" = "y" ] || [ "$input" = "Y" ]; then
        sudo pacman -S --noconfirm sudo pacman -S libvirt qemu virt-manager lxsession
        sudo pacman -S --noconfirm gst-plugins-good libvirt-runit

        sudo ln -s /etc/runit/sv/libvirtd /run/runit/service
        sudo ln -s /etc/runit/sv/virtlockd /run/runit/service
        sudo ln -s /etc/runit/sv/virtlogd /run/runit/service

        sudo usermod -G libvirt -a "$USER"

    fi



}

setup() {
    mkdir ~/docs/
    mkdir ~/dl/
    mkdir ~/media/
    mkdir -p ~/.local/share/gnupg/
    mkdir -p ~/.config/mpd/playlists
    sudo mkdir -p /etc/X11/xorg.conf.d/


    echo "use ufw defaults? (deny incoming, allow outgoing, add rules for steam and transmission ) (y/N)"
    read -r input
    if [ "$input" = 'y' ] || [ "$input" = 'Y' ]; then
    sudo ufw default deny incoming
    sudo ufw default allow outgoing
    sudo ufw allow http
    sudo ufw allow https
    #sudo ufw allow ssh
    sudo ufw allow ntp
    sudo ufw allow 67:68/tcp
    sudo ufw allow 53

    #allow torrent client traffic
    sudo ufw allow 56881:56889/tcp

    #rules to allow steam
    sudo ufw allow 27000:27036/udp
    sudo ufw allow 27036:27037/tcp
    sudo ufw allow 4380/udp

    sudo ufw enable
    fi

    #add wpa_supplicant hook to dhcpcd

    echo "setup wpa_supplicant (y/N)?"
    read -r input
    if [ "$input" = "y" ] || [ "$input" = "Y" ]; then
        #setup wpa_supplicant.conf
        if [ ! -f /etc/wpa_supplicant/wpa_supplicant.conf ]; then

            sudo mkdir -p /etc/wpa_supplicant/
            sudo cp ~/.config/.sysconf/etc/wpa_supplicant/wpa_supplicant.conf /etc/wpa_supplicant/

            sudo ln -s /usr/share/dhcpcd/hooks/10-wpa_supplicant /usr/lib/dhcpcd/dhcpcd-hooks/
        fi
    fi
    
    echo "setup amd graphics drivers?(y/N)"
    read -r amdin

    if [ "$amdin" = 'y' ] || [ "$amdin" = 'Y' ]; then
    #install amd libraries
        sudo ln -s ~/.config/.sysconf/etc/X11/xorg.conf.d/20-amdgpu.conf /etc/X11/xorg.conf.d/
        sudo pacman -S radeontop   #TODO arch specific

    fi

    clear
    echo "setup intel graphics drivers?(y/N)"
    read -r intelin

    if [ "$intelin" = 'y' ] || [ "$intelin" = 'Y' ]; then
        sudo ln -s ~/.config/.sysconf/etc/X11/xorg.conf.d/20-intel.conf /etc/X11/xorg.conf.d

    fi


    #install zsh shell
    clear
    echo "install and use zsh? (y/N)"
    read -r input
    if [ "$input" = 'y' ] || [ "$input" = 'Y' ]; then
        chsh -s /bin/zsh "$USER"

        #setup .zprofile and zsh history file
        cd ~ || return
        ln -s ~/.profile ~/.zprofile
        mkdir -p ~/.cache/zsh
        touch ~/.cache/zsh/history

    fi

    echo "disable mouse acceleration? (y/N)"
    read -r input
    if [ "$input" = 'y' ] || [ "$input" = 'Y' ]; then
        sudo ln -s ~/.config/.sysconf/etc/X11/xorg.conf.d/50-mouse-acceleration.conf /etc/X11/xorg.conf.d/
    fi

    echo "remove sudo and symlink doas to sudo, and give doas admin privilages to current user? (y/N)"  
    read -r input
    if [ "$input" = 'y' ] || [ "$input" = 'Y' ]; then
        echo "permit persist $USER as root" >> ~/.cache/doas.conf
        sudo cp ~/.cache/doas.conf /etc/doas.conf
        rm ~/.cache/doas.conf
        doas pacman -R sudo && doas ln -s /bin/doas /bin/sudo                            #TODO ARCH SPECIFIC
    fi



    #set limits for esync
    sudo sh -c "echo '$USER hard nofile 524288' >> /etc/security/limits.conf"

    sudo groupadd nogroup   #needed to use slock

    ### modify udevil config to add exec to mount options
    #sudo sed -i 's/^allowed_options .*$/&, exec/g' /etc/udevil/udevil.conf

    ### change dns to opendns for porn  blocking
    #sudo sh -c 'echo "static domain_name_servers=208.67.222.123 208.67.220.123" >> /etc/dhcpcd.conf'
    ### change dns to dns.watch, free speech dns
    #sudo sh -c 'echo "static domain_name_servers=84.200.69.80 84.200.70.40 2001:1608:10:25::1c04:b12f 2001:1608:10:25::9249:d69b" >> /etc/dhcpcd.conf'

    #if [ -f /bin/dash ]; then
    #    sudo rm /bin/sh
    #    sudo ln -s /bin/dash /bin/sh 
    #fi
    
    sh ~/.local/scripts/set-wp.sh ~/media/img/wallpapers/1483011234563.jpeg
    sh ~/.local/scripts/set-colors.sh ~/.config/colors/dracula

    mkdir -p ~/.config/sx/
    ln -s ~/.local/inits/dwm.sh ~/.config/sx/sxrc

}

[ "$USER" = "root" ] && echo "do not run as root/sudo" && exit

pkgm_install
setup

sh ~/.local/scripts/install.sh add-repos ~/.local/installers/repos.txt
