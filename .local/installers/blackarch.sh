sudo pacman -S xlockmore blackarch-config-x11

cp /usr/share/blackarch/config/x11/Xresources ~/.Xresources
cp /usr/share/blackarch/config/x11/Xdefaults ~/.Xdefaults

cp /etc/spectrwm/spectrwm_us.conf ~/.spectrwm.conf
cat /etc/spectrwm.conf | sed 29q >> ~/.spectrwm.conf
echo "tile_gap = 8" >> ~/.spectrwm.conf
echo "region_padding = 8" >> ~/.spectrwm.conf
