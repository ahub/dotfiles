#!/bin/sh
# this is intended to be run once you have entered the chroot
# modify variables below as needed

DSK=/dev/sda #if using nvme drive just changing this variable wont work due to partition naming differences

LOCALE="en_US.UTF-8"
TZ_CONTINENT="America"
TZ_CITY="CHICAGO"
HOSTNAME="4rt1x"
USERNAME="alex"



#setup locale
echo "$LOCALE UTF-8" >> /etc/locale.gen
locale-gen
echo "$LOCALE" > /etc/locale.conf
export LANG="$LOCALE"

#setup timezone and hostname
ln -s /usr/share/zoneinfo/"$TZ_CONTINENT"/"$TZ_CITY" /etc/localtime
#echo "hostname=4rt1x" > /etc/conf.d/hostname
echo "$HOSTNAME" > /etc/hostname

#modify /etc/mkinitcpio.conf for encrypted drive
sed -i "s/modconf block/modconf block encrypt lvm2 resume/g" /etc/mkinitcpio.conf

#setup file to autodecrypt, tried during first install and did not work, so I'm commenting out for now
#also have security concerns with using this, keeping in case I want to try it in the future

dd if=/dev/random of=/crypto_keyfile.bin bs=512 count=8 iflag=fullblock
chmod 000 /crypto_keyfile.bin
sed -i "s/FILES=(/FILES=(\/crypto_keyfile.bin/g" /etc/mkinitcpio.conf


pacman -S lvm2 lvm2-runit cryptsetup cryptsetup-runit

cryptsetup luksAddKey "$DSK"  /crypto_keyfile.bin #part of commented out code above

#build linux kernel
mkinitcpio -p linux-hardened

echo "Set the root user password:"
passwd

pacman -S grub efibootmgr
pacman -S dosfstools freetype2 fuse2 gptfdisk libisoburn mtools os-prober

#modify grub for encryption
sed -i "s/quiet/quiet resume=UUID=$(blkid -s UUID -o value /dev/lvmSystem/volSwap)/g" /etc/default/grub
sed -i "s/GRUB_CMDLINE_LINUX=\"\"/GRUB_CMDLINE_LINUX=\"cryptdevice=UUID=$(blkid -s UUID -o value /dev/nvme0n1p2):lvm-system\"/g" /etc/default/grub

sed -i "s/#GRUB_ENABLE_CRYPTODISK/GRUB_ENABLE_CRYPTODISK/g" /etc/default/grub
sed -i "s/#GRUB_COLOR_NORMAL/GRUB_COLOR_NORMAL/g" /etc/default/grub
sed -i "s/#GRUB_COLOR_HIGHLIGHT/GRUB_COLOR_HIGHLIGHT/g" /etc/default/grub

grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=artix --recheck "$DSK" 
grub-mkconfig -o /boot/grub/grub.cfg

#install extra necessary packages

sudo pacman -S dhcpcd dhcpcd-runit
pacman -S haveged haveged-runit
pacman -S cronie cronie-runit

sudo ln -s /etc/runit/sv/dhcpcd /etc/runit/runsvdir/default
sudo ln -s /etc/runit/sv/haveged /etc/runit/runsvdir/default
sudo ln -s /etc/runit/sv/cronie /etc/runit/runsvdir/default

#add normal user account

useradd -m -G wheel -s /bin/bash "$USERNAME"

echo "set password for user $USERNAME:"
passwd "$USERNAME"

#sudoers setup
sed -i "s/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/g" /etc/sudoers

echo "Installation Complete!"
echo "exit the chroot with the \"exit\" command"
echo "then unmount drives with \"umount -R /mnt\""
echo "and disable swap with \"swapoff -a\""
echo "then reboot with \"reboot\""
