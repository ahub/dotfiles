#!/bin/sh
#
# Taken from: https://www.rohlix.eu/post/artix-linux-full-disk-encryption-with-uefi/
# put into a script because I'm lazy
#
# $1 -> drive to install to ( sda, sdb, etc) (won't work on nvme for now due to different partition naming,  sda2 vs nvme0n1p2 


DSK=/dev/"$1"

MSG="pass name of drive to install to as first argument (sda, sdb,etc.). nvme drives will have issues due to partition naming differences"
! [ -f "$DSK" ] && echo "$MSG" && exit


pacman -s parted

#setup disks with parted
parted -s "$DSK" print

parted -s "$DSK" mklabel gpt

parted -s -a optimal "$DSK" mkpart "primary" "fat32" "0%" "512MiB"

parted -s "$DSK" set 1 esp on

parted -s "$DSK" align-check optimal 1

parted -s -a optimal "$DSK" mkpart "primary" "ext4" "512MiB" "100%"

parted -s "$DSK" set 2 lvm on


#cryptsetup commands

cryptsetup luksFormat -v --type=luks1 "$DSK"2

cryptsetup luksOpen "$DSK"2 lvm-system


#lvm partitioning
pvcreate /dev/mapper/lvm-system
vgcreate lvmSystem /dev/mapper/lvm-system

lvcreate -L 4G lvmSystem -n volSwap
lvcreate -l +100%FREE lvmSystem -n volRoot

mkswap /dev/lvmSystem/volSwap
mkfs.fat -n ESP -F 32  "$DSK"2
mkfs.ext4 -L volRoot /dev/lvmSystem/volRoot


#mount partitions
swapon /dev/lvmSystem/volSwap
mount /dev/lvmSystem/volRoot /mnt
mkdir -p /mnt/boot/efi
mount /dev/nvme0n1p1 /mnt/boot/efi


#install base system
basestrap /mnt base base-devel runit elogind-runit
basestrap /mnt linux-firmware linux-hardened linux-hardened-headers usbctl

#setup fstab
fstabgen -U /mnt >> /mnt/etc/fstab
echo "tmpfs	/tmp	tmpfs	rw,nosuid,noatime,nodev,size=4G,mode=1777	0 0" >> /mnt/etc/fstab


cp artix_crypt_install_chroot.sh /mnt/

echo "entering chroot of new Artix system.."
echo "open file artix_crypt_install_chroot.sh in the root directory and edit vars as needed, then run the script to complete installation"
#enter chroot
artix-chroot /mnt /bin/bash # formerly artools-chroot
