sudo setup-xorg-base
sudo apk add neovim
sudo ln -s /usr/bin/nvim /usr/bin/vim
sudo apk add git make gcc g++ libx11-dev libxft-dev libxinerama-dev ncurses dbus-x11 adwaita-gtk2-theme adwaita-icon-theme ttf-dejavu
sudo apk add webkit2gtk-dev
sudo apk add ncurses-dev
sudo apk add sxhkd dunst picom redshift mpd mpc ncmpcpp xautolock feh udevil
sudo apk add xrandr xclip
sudo apk add xf86-video-intel
sudo apk add sxiv
sudo apk add zip unzip p7zip unrar
sudo apk add font-noto-cjk
sudo apk add lynx

sudo apk add py3-pip clang
pip3 install --user pynvim

sudo apk add  transmission transmission-cli transmission-daemon

sudo apk add chromium

sudo apk add alsa-utils pulseaudio pulseaudio-blues pulseaudio-alsa alsa-plugins-pulse pulsemixer 

sudo apk add mpv youtube-dl

sudo apk add man-db
sudo apk add docs 

sudo apk add zathura zathra-pdf-mupdf
sudo apk add libxrandr-dev
sudo apk add slock
sudo apk add exa

sudo apk add wireless-tools
#sudo apk add wpa_supplicant wpa_supplicant-openrc
sudo apk add iwd iwd-openrc

sudo rc-update add iwd boot
sudo apk add xdg-utils
sudo apk add file
sudo apk add highlight

#add zathura-cb
sudo apk add libarchive libarchive-dev meson
sudo apk add zathura-dev

git clone https://github.com/pwmt/zathura-cb.git ~/devel/repos/zathura-cb
cd ~/devel/repos/zathura-cb
mkdir build
cd build
meson ..
sudo meson install

#install straw-viewer
sudo apk add perl-utils perl-module-build
sudo apk add perl-lwp-protocol-https perl-lwp-useragent-determined 
perl ./Build.pl
    

git clone https://github.com/trizen/straw-viewer.git ~/devel/repos/straw-viewer
cd ~/devel/repos/straw-viewer
perl Build.pl
./Build.pl installdeps
sudo ./Build.pl install

sudo ln -s /usr/bin/youtube-dl /bin/youtube-dl
sudo ln -s /usr/bin/mpv /bin/mpv


sudo apk add bluez pulseaudio-bluez
sudo rc-update add bluetooth
sudo rc-service bluetooth start

#install viu
sudo apk add cargo
cargo install viu

sudo apk add ffmpegthumbnailer

#flatpak
sudo apk add xdg-desktop-portal xdg-desktop-portal-gtk xdg-user-dirs
