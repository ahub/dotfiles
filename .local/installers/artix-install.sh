#!/usr/bin/env sh

#TODO 
#    - general cleanup/organization of package installation (put all packages to be installed in files?
#    - go through script and use the input flag variables to conditionally install/configure relevant things


err() {
    [ "$1" ] && echo "ERROR: $1"

    read line
}

base() {
    add_repos

    sudo pacman -Syu
    

#X11
    sudo pacman -S --noconfirm  xorg --ignore xorg-server-xdmx
    sudo pacman -S --noconfirm  xorg-drivers  xautolock sx #xorg-xinit
    sudo pacman -S --noconfirm  xwallpaper

#sound
    #sudo pacman -S --noconfirm  alsa-utils pulseaudio pulseaudio-bluetooth pulsemixer  #apulse
    sudo pacman -S --noconfirm pipewire pipewire-alsa pipewire-pulse pipewire-jack gst-plugin-pipewire
    sudo pacman -S --noconfirm pulsemixer pavucontrol alsa-utils

#misc libs/system utils
    sudo pacman -S --noconfirm  freetype2 apparmor gst-libav xdg-utils gconf xdg-user-dirs
    #sudo pacman -S --noconfirm  wpa_supplicant
    sudo pacman -S --noconfirm  fuse #to run appimages
    #sudo pacman -S --noconfirm  mtpfs #to mount android devices
    sudo pacman -S --noconfirm  python-urwid
    sudo pacman -S --noconfirm  xclip
    sudo pacman -S --noconfirm  btrfs-progs reiserfsprogs
 
    sudo pacman -S --noconfirm  wget curl 
    sudo pacman -S --noconfirm  zip unzip p7zip #unrar
    sudo pacman -S --noconfirm  base-devel pacman-contrib 
    sudo pacman -S --noconfirm  dash

    #background services 
    sudo pacman -S --noconfirm  chrony cronie bluez bluez-utils tlp
    sudo pacman -S --noconfirm  tlp-runit bluez-runit cronie-runit chrony-runit


#networking
    sudo pacman -S --noconfirm ufw dhcpcd wpa_supplicant connman connman-gtk 
    sudo pacman -S --noconfirm ufw-runit wpa_supplicant-runit dhcpcd-runit connman-runit

#background utilities
    sudo pacman -S --noconfirm  udevil picom redshift sxhkd dunst rsync

#setup graphics drivers
    sudo pacman -S  --noconfirm mesa

#install suckless deps
    sudo pacman -S --noconfirm  webkit2gtk gcr

#themes and fonts
    sudo pacman -S --noconfirm  noto-fonts-cjk noto-fonts-emoji unicode-emoji

    sudo pacman -S --noconfirm  fakeroot

#install yay aur helper
    mkdir -p ~/.local/src/
    git clone https://aur.archlinux.org/yay.git ~/.local/src/yay/ 
    cd ~/.local/src/yay || return
    makepkg -si

#link services
    #sudo ln -s /etc/runit/sv/tlp /run/runit/service/
    #sudo ln -s /etc/runit/sv/bluetoothd /run/runit/service/
    sudo ln -s /etc/runit/sv/ufw /run/runit/service/
    sudo ln -s /etc/runit/sv/cronie /run/runit/service/
    sudo ln -s /etc/runit/sv/chrony /run/runit/service/
    sudo ln -s /etc/runit/sv/connmand /run/runit/service/

    #sudo ln -s /etc/runit/sv/iwd /run/runit/service/

    if [ "$NVIDIA_GPU" ]; then
        sudo mkdir -p /etc/pacman.d/hooks/
        sudo cp ~/.config/.sysconf/etc/pacman.d/hooks/nvidia.hook /etc/pacman.d/hooks/
        sudo pacman -S --noconfirm   nvidia nvidia-settings nvtop
    fi

}


extra() {
#misc programs I use with lf for previews or with scripts (~/.local/scripts/)
    sudo pacman -S --noconfirm  imagemagick ffmpegthumbnailer highlight python-pdftotext viu
    sudo pacman -S --noconfirm  python-pywal


#User programs
    sudo pacman -S --noconfirm  zathura zathura-cb zathura-pdf-mupdf zathura-djvu
    sudo pacman -S --noconfirm  youtube-dl transmission-cli
    sudo pacman -S --noconfirm  termdown scrot sxiv
    sudo pacman -S --noconfirm  neovim
    sudo pacman -S --noconfirm  mpd ncmpcpp mpc mpv

    sudo pacman -S --noconfirm   opendoas zsh lynx

    sudo pacman -S --noconfirm lxappearance qt5ct  #configuation utilities for gtk and qt
    #sudo pacman -S --noconfirm librewolf

#muttwizard dependancies
    sudo pacman -S --noconfirm  neomutt isync msmtp pass
#install reqs for deoplete in nvim
    sudo pacman -S --noconfirm  python-pip clang
    #pip3 install --user pynvim #getting errors using this
    sudo pacman -S --noconfirm python-pynvim

    
#install aur packages
    #yay -S librewolf-bin
    yay -S lf
    yay -S mutt-wizard
    #yay -S htim-git #giving incorrect statistics
    yay -S jmtpfs # to mount android phones
    yay -S pipe-viewer-git
    yay -S deb2appimage

    #yay -Ssfeed-git sfeed-curses-git
    yay -S tremc-git
    yay -S dashbinsh
    #yay -Sherbe-git tiramisu-git

#symlink vim to neovim
    sudo ln -s /bin/nvim /bin/vim

#install programs from extra repositories
    if [ -z "$(grep "chaotic-aur" /etc/pacman.conf)" ]; then
        #sudo pacman -S --no-confirm brave
        sudo pacman -S chaotic-aur/ungoogled-chromium
        yay -S aur/chromium-widevine
        
    fi

}

devel() {

#development
    echo "install Haskell? (y/N)?"
    read -r input
    if [ "$input" = 'y' ] || [ "$input" = 'Y' ]; then
        sudo pacman -S --noconfirm ghc cabal-install stack
    fi

    sudo pacman -S --noconfirm shellcheck

    echo "install virt-manager?(y/N)"
    read -r input
    if [ "$input" = "y" ] || [ "$input" = "Y" ]; then
        sudo pacman -S --noconfirm sudo pacman -S libvirt qemu virt-manager lxsession
        sudo pacman -S --noconfirm gst-plugins-good libvirt-runit

        sudo ln -s /etc/runit/sv/libvirtd /run/runit/service
        sudo ln -s /etc/runit/sv/virtlockd /run/runit/service
        sudo ln -s /etc/runit/sv/virtlogd /run/runit/service

        sudo usermod -G libvirt -a "$USER"

    fi

}

gaming() {

#Gaming NEED TO HAVE LIB-32 AND MULTILIB UNCOMMENTED IN /etc/pacman.conf (both lines. line w/ Include and [lib32])
  #wine and deps

    #check if lib32 is enabled
    if [ "$(grep "^\[lib32" /etc/pacman.conf)" ]; then 

        [ "$NVIDIA_GPU" ] && sudo pacman -S --noconfirm nvidia-utils lib32-nvidia-utils
        [ "$AMD_GPU" ] && sudo pacman -S --noconfirm vulkan-radeon lib32-vulkan-radeon  
        [ "$INTEL_GPU" ] && sudo pacman -S --noconfirm vulkan-intel lib32-vulkan-intel

        sudo pacman -S vulkan-mesa-layers lib32-vulkan-mesa-layers

        sudo pacman -S --noconfirm  wine-staging winetricks
        sudo pacman -S  --noconfirm giflib lib32-giflib libpng lib32-libpng libldap lib32-libldap gnutls lib32-gnutls mpg123 lib32-mpg123 openal lib32-openal v4l-utils lib32-v4l-utils libpulse lib32-libpulse alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo libxcomposite lib32-libxcomposite libxinerama lib32-libxinerama ncurses lib32-ncurses opencl-icd-loader lib32-opencl-icd-loader libxslt lib32-libxslt libva lib32-libva gtk3 lib32-gtk3 gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader cups samba dosbox

        sudo pacman -S --noconfirm gamemode lib32-gamemode


        sudo pacman -S --noconfirm lutris steam

    else
        err "lib32 is not enabled. Gaming programs not installed. be sure to run add_repos()" 
    fi

}


alt_wm_installs() {

    #install spectrwm packages
    #sudo pacman -S --noconfirm  spectrwm dmenu xlockmore alacritty
    #sudo ln -s ~/.local/inits/spectrwm.sh ~/.xinitrc

    echo "install xfce?(y/N)"
    read -r input
    if [ "$input" = "y" ] || [ "$input" = "Y" ]; then
        #install xfce packages
        sudo pacman -S --noconfirm xfce4 xfce4-goodies
    fi

    echo "install river and wayland utils?(y/N)"
    read -r input
    if [ "$input" = "y" ] || [ "$input" = "Y" ]; then
        sudo pacman -S wlroots wayland-protocols

        #utils for backgrounds run prompt, etc.
        sudo pacman -S bemenu-wayland alacritty inotify-tools swaybg imv wl-clipboard mako pamixer swayidle swaylock
        #aur programs
        yay -S wlr-randr wlsunset

        #install river and waybar. using waybar from git right now because pacman version did not work with tags
        yay -S river-git waybar-git

     
        #fix error when running river
        sudo sh -c 'echo "LIBSEAT_BACKEND=logind" >> /etc/environment'

        
    fi

    echo "install and enable lightdm?(y/N)"
    read -r input
    if [ "$input" = "y" ] || [ "$input" = "Y" ]; then
        sudo pacman -S --noconfirm lightdm lightdm-runit lightdm-gtk-greeter lightdm-gtk-greeter-settings
        sudo ln -s /etc/runit/sv/lightdm /run/runit/service/
        sudo cp -r ~/.local/share/themes/Nordic-standard-buttons /usr/share/themes
        sudo cp -r ~/.local/share/icons/Papirus-Dark /usr/share/icons/
    fi


    #setup ly display manager TODO get this working
    #yay -S ly
    #echo "#!/bin/sh" > ~/.xprofile
    #echo ". ~/.local/inits/startup.sh" >> ~/.xprofile
    #echo "startup" >> ~/.xprofile


}


setup() {
    mkdir ~/docs/
    mkdir ~/dl/
    mkdir ~/media/
    mkdir -p ~/.local/share/gnupg/
    mkdir -p ~/.config/mpd/playlists
    sudo mkdir -p /etc/X11/xorg.conf.d/


    if [ "$UFW_CONFIG" ]; then
        sudo ufw default deny incoming
        sudo ufw default allow outgoing
        sudo ufw allow http
        sudo ufw allow https
        #sudo ufw allow ssh
        sudo ufw allow ntp
        sudo ufw allow 67:68/tcp
        sudo ufw allow 53

        #allow torrent client traffic
        sudo ufw allow 56881:56889/tcp

        #rules to allow steam
        sudo ufw allow 27000:27036/udp
        sudo ufw allow 27036:27037/tcp
        sudo ufw allow 4380/udp

        sudo ufw enable
    fi

    #add wpa_supplicant hook to dhcpcd
        #setup wpa_supplicant.conf
    #    if [ ! -f /etc/wpa_supplicant/wpa_supplicant.conf ]; then

    #        sudo mkdir -p /etc/wpa_supplicant/
    #        sudo cp ~/.config/.sysconf/etc/wpa_supplicant/wpa_supplicant.conf /etc/wpa_supplicant/

    #        sudo ln -s /usr/share/dhcpcd/hooks/10-wpa_supplicant /usr/lib/dhcpcd/dhcpcd-hooks/
    #    fi
    
    if [ "$AMD_GPU" ]; then
    #install amd libraries
        sudo ln -s ~/.config/.sysconf/etc/X11/xorg.conf.d/20-amdgpu.conf /etc/X11/xorg.conf.d/
        sudo pacman -S radeontop   #TODO arch specific

    fi

    if [ "$INTEL_GPU" ]; then
        sudo ln -s ~/.config/.sysconf/etc/X11/xorg.conf.d/20-intel.conf /etc/X11/xorg.conf.d

    fi


    #install zsh shell
    if [ "$ZSH_SETUP" ]; then
        chsh -s /bin/zsh "$USER"

        #setup .zprofile and zsh history file
        cd ~ || return
        ln -s ~/.profile ~/.zprofile
        mkdir -p ~/.cache/zsh
        touch ~/.cache/zsh/history

    fi

    if [ "$MOUSE_ACCEL" ]; then
        sudo ln -s ~/.config/.sysconf/etc/X11/xorg.conf.d/50-mouse-acceleration.conf /etc/X11/xorg.conf.d/
    fi

    if [ "$DOAS_SETUP" ]; then
        echo "permit persist $USER as root" >> ~/.cache/doas.conf
        echo "permit persist $USER cmd radeontop" >> ~/.cache/doas.conf
        echo "permit persist $USER cmd poweroff" >> ~/.cache/doas.conf
        echo "permit persist $USER cmd restart" >> ~/.cache/doas.conf
        
        doas -C ~/.cache/doas.conf pacman -R sudo && doas ln -s /bin/doas /bin/sudo && sudo cp ~/.cache/doas.conf /etc/doas.conf && rm ~/.cache/doas.conf                        #TODO ARCH SPECIFIC
    fi

    #set console font
    sudo cp ~/.config/.sysconf/etc/vconsole.conf /etc/


    #set limits for esync
    sudo sh -c "echo '$USER hard nofile 524288' >> /etc/security/limits.conf"

    #set limits for monero
    sudo sh -c "echo '$USER hard memlock 2048' >> /etc/security/limits.conf"
    sudo sh -c "echo '$USER hard memlock 2048' >> /etc/security/limits.conf"

    sudo groupadd nogroup   #needed to use slock

    ### modify udevil config to add exec to mount options
    #sudo sed -i 's/^allowed_options .*$/&, exec/g' /etc/udevil/udevil.conf

    #if [ -f /bin/dash ]; then
    #    sudo rm /bin/sh
    #    sudo ln -s /bin/dash /bin/sh 
    #fi

    #fix issue with arduino ide and tiling wms
    sudo sh -c 'echo "export _JAVA_AWT_WM_NONREPARENTING=1" >> /etc/profile.d/jre.sh'

    #set console terminal font
    sudo sed 's/FONT=.*$/FONT=Lat2-Terminus16/g' /etc/vconsole.conf

    
    #set wallpaper and color scheme
    sh ~/.local/scripts/set-wp.sh ~/media/img/wallpapers/1483011234563.jpeg
    sh ~/.local/scripts/set-colors.sh ~/.config/colors/dracula

    #setup sx
    mkdir -p ~/.config/sx/
    ln -s ~/.local/inits/dwm.sh ~/.config/sx/sxrc

}


MOUSE_ACCEL=""
NVIDIA_GPU=""
AMD_GPU=""
INTEL_GPU=""
GAMER=""
UFW_CONFIG=""
ZSH_SETUP=""
DOAS_SETUP=""
NO_BASE=""
NO_EXTRA=""
NO_EXTRA_REPOS=""
NO_SETUP=""
ALT_WMS=""

#parse all arguments and set variables accordingly
for arg in "$@"
do
    [ "$arg" = "--mouse-flat" ] && MOUSE_ACCEL="true"
    [ "$arg" = "--nvidiagpu" ] && NVIDIA_GPU="true"
    [ "$arg" = "--amdgpu" ] && AMD_GPU="true"
    [ "$arg" = "--intelgpu" ] && AMD_GPU="true"
    [ "$arg" = "--gamer" ] && GAMER="true"
    [ "$arg" = "--ufw" ] && UFW_CONFIG="true"
    [ "$arg" = "--zsh" ] && ZSH_SETUP="true"
    [ "$arg" = "--doas" ] && DOAS_SETUP="true"
    [ "$arg" = "--no-base" ] && NO_BASE="true"
    [ "$arg" = "--no-extra" ] && NO_EXTRA="true"
    [ "$arg" = "--no-extra-repos" ] && NO_EXTRA_REPOS="true"
    [ "$arg" = "--no-setup" ] && NO_SETUP="true"
    [ "$arg" = "--alt_wms" ] && ALT_WMS="true"


    #define the --help display here
    if [ "$arg" = "--help" ]; then
        echo "Install script for Artix Linux."  
        echo "Usage: ./artix-install.sh [OPTIONS]"
        echo ""
        echo "    --mouse-flat       =>    disable mouse acceleration" 
        echo "    --nvidiagpu        =>    setup with nvidia proprietary graphics driver" 
        echo "    --amdgpu           =>    setup with amd radeon graphics driver"
        echo "    --intelgpu         =>    setup with intel graphics driver" 
        echo "    --gamer            =>    install lutris wine and steam" 
        echo "    --ufw              =>    setup ufw with my defaults (allow transmission and steam)"
        echo "    --zsh              =>    switch shell to zsh"
        echo "    --doas             =>    install doas, uninstall sudo (using doas), and symlink doas to sudo" 
        echo "    --help             =>    show this help menu"
        echo "    --no-base          =>    skip base package install. Should only be done if base has already been installed previously"
        echo "    --no-extra         =>    skip installing extra packages I personally use."
        echo "    --no-extra-repos   =>    skip enabling extra repositories (lib32, arch repos, universe, etc) will cause some packages to not install."
        echo "    --no-setup         =>    skip my post-install configuration. Setting up directories, etc."
        echo "    --alt-wms          =>    display prompts to install alternate window managers/display managers like xfce, spectrwm, etc."
        echo ""

        exit
    fi

done


echo "MOUSE_ACCEL    = $MOUSE_ACCEL"
echo "NVIDIA_GPU     = $NVIDIA_GPU"
echo "AMD_GPU        = $AMD_GPU"
echo "INTEL_GPU      = $INTEL_GPU"
echo "GAMER          = $GAMER"
echo "UFW_CONFIG     = $UFW_CONFIG"
echo "ZSH_SETUP      = $ZSH_SETUP"
echo "DOAS_SETUP     = $DOAS_SETUP"
echo "NO_BASE        = $NO_BASE"
echo "NO_EXTRA       = $NO_EXTRA"
echo "NO_EXTRA_REPOS = $NO_EXTRA_REPOS"
echo "NO_SETUP       = $NO_SETUP"
echo "ALT_WMS        = $ALT_WMS"
echo ""
echo "Current flags set. Press enter to continue."
read input


#script is not meant to be run as root, it pulls the $USER variable for some commands, and sets things up in the user's home directory
[ "$USER" = "root" ] && echo "do not run as root/sudo" && exit

[ -z "$NO_EXTRA_REPOS" ] && sh ~/.local/installers/artix-extra-repos.sh 
[ -z "$NO_BASE" ] && base
[ -z "$NO_EXTRA" ] && extra
[ -z "$NO_SETUP" ] && setup

[ "$GAMER" ] && gamer
[ "$ALT_WMS" ] && alt_wms

sh ~/.local/scripts/install.sh add-repos ~/.local/installers/repos.txt
