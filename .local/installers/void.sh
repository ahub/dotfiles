#!/bin/sh

#core programs always needed
core() {
    #Xorg
    sudo xbps-install -Sy xorg-minimal xrdb xsetroot xterm xprop xrandr xclip xkill xautolock

    #sound
    sudo xbps-install -Sy alsa-utils pulseaudio pulsemixer

    #libs/utils needed by programs used
    sudo xbps-install -Sy freetype gst-libav xdg-utils GConf

    #needed for appimages to work
    sudo xbps-install -Sy dbus-glib libatomic

    #system utils (provide basic functionality expected from an OS: wifi, bluetooth, etc)
    sudo xbps-install -Sy chrony bluez iwd elogind scron ufw zsh

    #cli utilities (cli utilities I use indirectly through scripts etc, some things may not work if these are not installed) 
    sudo xbps-install -Sy curl rsync 

    #archive utilities
    sudo xbps-install -Sy zip unzip p7zip p7zip-rar zstd  

    #background utilities (background utilities run at startup from .xinitrc
    sudo xpbs-install -Sy xwallpaper udevil picom redshift sxhkd

    #suckless dependancies
    sudo xbps-install -S base-devel make libXft-devel libX11-devel libXinerama-devel patch ncurses-devel
    sudo xbps-install -S gtk+-devel webkit2gtk-devel gcr-devel #surf dependencies


    #install graphics libraries for amd/intel
	sudo xbps-install -Sy xf86-video-amdgpu mesa-vulkan-radeon
	sudo xbps-install -Sy xf86-video-intel mesa-dri intel-video-accel	

}

#extra programs that are usually used, but not necessary (theming, user programs, etc)
extra() {
    #lf utilities, provide previews for more filetypes, better previews, etc.
    sudo xbps-install -Sy ffmpegthumbnailer highlight viu #ImageMagick jp2a

    #fonts and theming
    sudo xbps-install -Sy  noto-fonts-ttf noto-fonts-ttf-extra noto-fonts-cjk font-awesome
    sudo xbps-install -Sy pywal

    #user cli programs 
    sudo xbps-install -Sy neovim htop ncmpcpp lf youtube-dl straw-viewer tremc tealdeer termdown sfeed_curses 
    sudo xbps-install -Sy mpd transmission scrot sfeed 

    #user gui programs
    sudo xbps-install -Sy mpv firefox
    sudo xbps-install -Sy zathura zathura-cb zathura-pdf-mupdf zathura-djvu zathura-ps

    #WINDOW MANAGERS
    #spectrwm
    echo "install spectrwm and associated programs? (y/N)"
    read input
    [ "$input" = "y" ] || [ "$input" = "Y" ] && sudo xbps-install -Sy spectrwm xlockmore dmenu alacritty

    #virt-manager programs
    sudo xbps-install -Sy libvirt qemu virt-manager lxsession gst-plugins-good1

    #mutt-wizard dependancies
    sudo xbps-install -Sy neomutt isync msmtp pass urlview lynx notmuch

    clear
    echo "install flatpak and setup flathub?(y/N)"
    read input
    if [ $input = 'y' ]; then
        #install flatpak and enable flathub
        sudo xbps-install -Sy xdg-desktop-portal xdg-desktop-portal-gtk xdg-user-dirs xdg-user-dirs-gtk xdg-utils
        sudo xbps-install -Sy flatpak
        sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    fi

}

nonfree() {
    #enable non-free repos
    sudo xbps-install -Sy void-repo-nonfree
    sudo xbps-install -Sy void-repo-multilib
    sudo xbps-install -Sy void-repo-multilib-nonfree


    #install nvidia-drivers
    clear
    echo "install nvidia proprietary drivers? (Y/n)"
    read nvidiain
    if [ $nvidiain = 'y' ]; then
        sudo xbps-install -Sy void-repo-nonfree void-repo-multilib void-repo-multilib-nonfree 
        sudo xbps-install -Sy xf86-video-nouveau nvidia nvidia-libs-32bit
        sudo xbps-install vulkan-loader vulkan-loader-32bit
    fi



    clear
    echo "install gaming programs(wine lutris steam)? (y/N)"
    echo "this will enable nonfree repos"
    read input
    if [ $input = 'y' ]; then
        sudo xbps-install -Sy void-repo-nonfree void-repo-multilib void-repo-multilib-nonfree
        sudo xbps-install -Sy mesa-dri-32bit
        sudo xbps-install -Sy wine-32bit wine-gecko wine-mono winetricks protontricks libselinux
        sudo xbps-install -Sy lutris steam
        sudo xbps-install -Sy libdrm-32bit
    fi

}

config() {
    #make directories
    mkdir ~/docs/
    mkdir ~/dl/
    mkdir ~/media/
    mkdir -p ~/.local/share/gnupg/
    mkdir -p ~/.config/pki/nssdb/
    mkdir -p ~/.local/share/pki/nssdb/
    sudo mkdir -p /etc/X11/xorg.conf.d/

#setup ufw with my default settings, allow steam and transmission through
    sudo ufw default deny incoming
    sudo ufw default allow outgoing
    sudo ufw allow http
    sudo ufw allow https
    #sudo ufw allow ssh
    sudo ufw allow ntp
    sudo ufw allow 67:68/tcp
    sudo ufw allow 53

    #allow torrent client traffic
    sudo ufw allow 56881:56889/tcp

    #rules to allow steam
    sudo ufw allow 27000:27036/udp
    sudo ufw allow 27036:27037/tcp
    sudo ufw allow 4380/udp

    sudo ufw enable

    #change shell to zsh, setup .zprofile and zsh history file
	cd ~
	ln -s ~/.profile ~/.zprofile
	mkdir -p ~/.cache/zsh
	touch ~/.cache/zsh/history
    chsh -s /bin/zsh $USER 

    #disable mouse acceleration 
	sudo ln -s ~/.config/.sysconf/etc/X11/xorg.conf.d/50-mouse-acceleration.conf /etc/X11/xorg.conf.d/

    # modify udevil config to add exec to mount options TODO remove?
    sudo sed -i 's/^allowed_options .*$/&, exec/g' /etc/udevil/udevil.conf
    
    # set limits.conf for esync
    sudo sh -c "echo '$USER hard nofile 524288' >> /etc/security/limits.conf"

    #setup virt-manager
    sudo usermod -G libvirt -a $USER
    #sudo usermod -G libvirt -a root #may not be needed 

    #set ~/.config/color
    sh ~/.local/scripts/set-colors.sh ~/.config/colors/dracula


}

distro_specific_config() {

    sudo xbps-install -Sy outils void-release-keys #provides way to verify install iso 

    #enable usage statistics
    clear
    echo "enable usage statistics for Void Linux? (y/N)"
    read input
    if [ $input = 'y' ]; then
        sudo xbps-install -Sy PopCorn
        sudo ln -s /etc/sv/popcorn /var/service/
    fi


    echo "change mirror to alpha.us.voidlinux.org? (y/N)"
    read input
    if [ $input = 'y' ]; then
        sudo mkdir -p /etc/xbps.d
        sudo cp /usr/share/xbps.d/*-repository-*.conf /etc/xbps.d/
        sudo sed -i 's|https://alpha.de.repo.voidlinux.org|https://alpha.us.repo.voidlinux.org|g' /etc/xbps.d/*-repository-*.conf
        sudo xbps-install -S
    fi

    mkdir -p ~/devel/repos/
    git clone https://github.com/toluschr/xdeb.git ~/devel/repos/xdeb
    cd ~/devel/repos/xdeb
    sudo cp ./xdeb /usr/local/bin


    clear
    echo "enable wifi with IWD? (use iwctl to setup network) (y/N)"
    read input
    if [ $input = 'y' ];  then
        sudo ln -s /etc/sv/iwd /var/service/
    fi

    #enable virt-manager services
    sudo ln -s /etc/sv/libvirtd /var/service
    sudo ln -s /etc/sv/virtlockd /var/service
    sudo ln -s /etc/sv/virtlogd /var/service

    #enable services
    sudo ln -s /etc/sv/dbus /var/service
    #sudo ln -s /etc/sv/cgmanager /var/service/
    sudo ln -s /etc/sv/elogind /var/service/
    sudo ln -s /etc/sv/chronyd /var/service/

}

#tools I use for personal programming projects
devel() {
   #Haskell
    #sudo xbps-install -Sy ghc cabal-install stack
    sudo xbps-install -Sy gmp-devel ncurses-libtinfo-libs

    #install ghcup and cabal 
    curl --proto '=https' --tlsv1.2 -sSf https://get-ghcup.haskell.org | sh

    #install stack
    curl -sSL https://get.haskellstack.org/ | sh

    #C/C++
    sudo xbps-install -Sy base-devel gcc gdb

    #Lisp
    #sudo xbps-install -Sy clisp

   

}
