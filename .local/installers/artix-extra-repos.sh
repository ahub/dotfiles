#!/bin/sh
if [ -z "$(grep "^\[lib32" /etc/pacman.conf)" ]; then
    echo "lib32 not enabled. Appending lines to /etc/pacman.conf"
    sleep 1


    #TODO lib32 is not found on fresh install, need to modify pacman.conf
    sudo sh -c 'echo "[lib32]" >> /etc/pacman.conf'
    sudo sh -c 'echo "Include = /etc/pacman.d/mirrorlist" >> /etc/pacman.conf'
    sudo sh -c 'echo "" >> /etc/pacman.conf'


    sudo pacman -Sy

fi

#add arch repos and refresh keyrings, had issues if done before lib32
    sudo pacman -S artix-archlinux-support lib32-artix-archlinux-support
    sudo pacman-key --populate archlinux
    sudo pacman -Sy


if [ -z "$(grep "community" /etc/pacman.conf)" ]; then
    
        sudo sh -c 'echo "[extra]" >> /etc/pacman.conf'
        sudo sh -c 'echo "Include = /etc/pacman.d/mirrorlist-arch" >> /etc/pacman.conf'
        sudo sh -c 'echo "" >> /etc/pacman.conf'
        sudo sh -c 'echo "[community]" >> /etc/pacman.conf'
        sudo sh -c 'echo "Include = /etc/pacman.d/mirrorlist-arch" >> /etc/pacman.conf'
        sudo sh -c  'echo "" >> /etc/pacman.conf'
        sudo sh -c 'echo "[multilib]" >> /etc/pacman.conf'
        sudo sh -c 'echo "Include = /etc/pacman.d/mirrorlist-arch" >> /etc/pacman.conf'
fi        

#add universe repo

if [ -z "$(grep "universe" /etc/pacman.conf)" ]; then
    sudo sh -c 'echo "[universe]" >> /etc/pacman.conf'

    sudo sh -c 'echo "Server = https://universe.artixlinux.org/\$arch" >> /etc/pacman.conf'
    sudo sh -c 'echo "Server = https://mirror1.artixlinux.org/universe/\$arch" >> /etc/pacman.conf'
    sudo sh -c 'echo "Server = https://mirror.pascalpuffke.de/artix-universe/\$arch" >> /etc/pacman.conf'
    sudo sh -c 'echo "Server = https://artixlinux.qontinuum.space:4443/universe/os/" >> /etc/pacman.conf'
    sudo sh -c 'echo "Server = https://mirror1.cl.netactuate.com/artix/universe/\$arch" >> /etc/pacman.conf'

fi


if [ -z "$(grep "chaotic-aur" /etc/pacman.conf)" ]; then
    sudo pacman-key --recv-key FBA220DFC880C036 --keyserver keyserver.ubuntu.com
    sudo pacman-key --lsign-key FBA220DFC880C036
    sudo pacman -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'

    sudo sh -c 'echo "[chaotic-aur]" >> /etc/pacman.conf'
    sudo sh -c 'echo "Include = /etc/pacman.d/chaotic-mirrorlist" >> /etc/pacman.conf'
fi
