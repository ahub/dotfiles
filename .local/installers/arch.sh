#!/usr/bin/env sh

pkgm_install() {
#X11
    sudo pacman -S  xorg --ignore xorg-server-xdmx
    sudo pacman -S  xorg-drivers xorg-xinit xautolock 
    sudo pacman -S  xwallpaper 

#sound
    sudo pacman -S  alsa-utils pulseaudio pulseaudio-bluetooth pulsemixer  #apulse

#misc libs/system utils
    sudo pacman -S  freetype2 apparmor gst-libav xdg-utils gconf
    #sudo pacman -S  wpa_supplicant
    sudo pacman -S  fuse #to run appimages
    #sudo pacman -S  mtpfs #to mount android devices
    sudo pacman -S  python-urwid gst-libav xdg-utils
    sudo pacman -S  wget curl chrony bluez bluez-utils ufw tlp
    sudo pacman -S  zip unzip #unrar
    sudo pacman -S  base-devel

#background utilities
    sudo pacman -S  udevil picom redshift sxhkd rsync cronie

#misc programs I use with lf for previews or with scripts (~/.local/scripts/)
    sudo pacman -S  imagemagick ffmpegthumbnailer highlight python-pdftotext viu
    sudo pacman -S  python-pywal


#User programs
    sudo pacman -S  zathura zathura-cb zathura-pdf-mupdf zathura-djvu 
    sudo pacman -S  youtube-dl transmission-cli
    sudo pacman -S  termdown scrot sxiv
    sudo pacman -S  neovim
    sudo pacman -S  mpd ncmpcpp mpc mpv

    sudo pacman -S   opendoas zsh lynx

#Gaming NEED TO HAVE LIB-32 AND MULTILIB UNCOMMENTED IN /etc/pacman.conf (both lines. line w/ Include and [lib32])
  #wine and deps
    clear
    echo "install gamer programs? NEED TO HAVE LIB-32 AND MULTILIB UNCOMMENTED IN /etc/pacman.conf (both lines. line w/ Include and [lib32]) (y/N)"
    read input
    if [ $input == 'y' ] || [ $input == 'Y' ]; then
    sudo neovim /etc/pacman.conf
    sudo pacman -S  wine-staging winetricks wine-gecko wine-mono
    sudo pacman -S  giflib lib32-giflib libpng lib32-libpng libldap lib32-libldap gnutls lib32-gnutls mpg123 lib32-mpg123 openal lib32-openal v4l-utils lib32-v4l-utils libpulse lib32-libpulse alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo libxcomposite lib32-libxcomposite libxinerama lib32-libxinerama ncurses lib32-ncurses opencl-icd-loader lib32-opencl-icd-loader libxslt lib32-libxslt libva lib32-libva gtk3 lib32-gtk3 gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader cups samba dosbox


    sudo pacman -S  lutris steam
    fi

#themes and fonts
    sudo pacman -S  noto-fonts-cjk

#muttwizard dependancies
    sudo pacman -S  neomutt isync msmtp pass

#setup graphics drivers
    sudo pacman -S  mesa

#install suckless deps
    sudo pacman -S  webkit2gtk gcr

#install reqs for deoplete in nvim
    sudo pacman -S  python-pip clang
    pip3 install --user pynvim

#install spectrwm packages
    sudo pacman -S  spectrwm dmenu xlockmore alacritty


#install yay aur helper
    sudo pacman -S  fakeroot make
    mkdir -p ~/.local/src/
    git clone https://aur.archlinux.org/yay.git ~/devel/repos/yay/ 
    cd ~/devel/repos/yay
    makepkg -si

#install aur packages
    yay -S  librewolf-bin
    yay -S  lf
    yay -S  mutt-wizard
    yay -S  htop-vim-git
    yay -S  jmtpfs # to mount android phones
    yay -S  straw-viewer-git
    yay -S  deb2appimage
    yay -S  sfeed-git sfeed-curses-git
    yay -S tremc-git
    yay -S herbe-git tiramisu-git

#symlink vim to neovim
    sudo ln -s /bin/nvim /bin/vim



#link services
    #sudo ln -s /etc/runit/sv/tlp /etc/runit/runsvdir/default/
    #sudo ln -s /etc/runit/sv/bluetoothd /etc/runit/runsvdir/default/
    #sudo ln -s /etc/runit/sv/ufw /etc/runit/runsvdir/default/
    #sudo ln -s /etc/runit/sv/cronie /etc/runit/runsvdir/default
    sudo systemctl enable ufw
    sudo systemctl enable cronie

#setup ly display manager
    yay -S  ly
    sudo systemctl enable ly
    sudo systemctl disable getty@tty2.service
    echo "#!/bin/sh" > ~/.xprofile
    echo ". ~/.local/inits/startup.sh" >> ~/.xprofile
    echo "startup" >> ~/.xprofile



    echo "install nvidia proprietary drivers?(y/N)"
    read input
    if [ "$input" = "y" ] || [ "$input" = "Y" ]; then
	    sudo cp ~/.config/.sysconf/etc/pacman.d/hooks/nvidia.hook /etc/pacman.d/hooks/
	    sudo pacman -S  nvidia nvidia-settings nvtop
    fi

}

setup() {
    mkdir ~/docs/
    mkdir ~/dl/
    mkdir ~/media/
    mkdir -p ~/.local/share/gnupg/
    mkdir -p ~/.config/mpd/playlists
    sudo mkdir -p /etc/X11/xorg.conf.d/


    echo "use ufw defaults? (deny incoming, allow outgoing, add rules for steam and transmission ) (y/N)"
    read input
    if [ $input == 'y' ] || [ $input == 'Y' ]; then
    sudo ufw default deny incoming
    sudo ufw default allow outgoing
    sudo ufw allow http
    sudo ufw allow https
    #sudo ufw allow ssh
    sudo ufw allow ntp
    sudo ufw allow 67:68/tcp
    sudo ufw allow 53

    #allow torrent client traffic
    sudo ufw allow 56881:56889/tcp

    #rules to allow steam
    sudo ufw allow 27000:27036/udp
    sudo ufw allow 27036:27037/tcp
    sudo ufw allow 4380/udp

    sudo ufw enable
    fi

    #add wpa_supplicant hook to dhcpcd
    sudo ln -s /usr/share/dhcpcd/hooks/10-wpa_supplicant /usr/lib/dhcpcd/dhcpcd-hooks/

    #setup wpa_supplicant.conf
    if [ ! -f /etc/wpa_supplicant/wpa_supplicant.conf ]; then
        sudo mkdir -p /etc/wpa_supplicant/
        sudo cp ~/.config/.sysconf/etc/wpa_supplicant/wpa_supplicant.conf /etc/wpa_supplicant/
    fi
    
    echo "setup amd graphics drivers?(y/N)"
    read amdin

    if [ $amdin == 'y' ] || [ $amdin == 'Y' ]; then
    #install amd libraries
        sudo ln -s ~/.config/.sysconf/etc/X11/xorg.conf.d/20-amdgpu.conf /etc/X11/xorg.conf.d/

    fi

    clear
    echo "setup intel graphics drivers?(y/N)"
    read intelin

    if [ $intelin == 'y' ] || [ $intelin == 'Y' ]; then
        sudo ln -s ~/.config/.sysconf/etc/X11/xorg.conf.d/20-intel.conf /etc/X11/xorg.conf.d

    fi


    #install zsh shell
    clear
    echo "install and use zsh? (y/N)"
    read input
    if [ $input == 'y' ] || [ $input == 'Y' ]; then
        chsh -s /bin/zsh $USER

        #setup .zprofile and zsh history file
        cd ~
        ln -s ~/.profile ~/.zprofile
        mkdir -p ~/.cache/zsh
        touch ~/.cache/zsh/history

    fi

    echo "disable mouse acceleration? (y/N)"
    read input
    if [ $input == 'y' ] || [ $input == 'Y' ]; then
        sudo ln -s ~/.config/.sysconf/etc/X11/xorg.conf.d/50-mouse-acceleration.conf /etc/X11/xorg.conf.d/
    fi

    echo "remove sudo and symlink doas to sudo, and give doas admin privilages to current user? (y/N)"  
    read input
    if [ $input == 'y' ] || [ $input == 'Y' ]; then
        echo "permit persist $USER as root" >> ~/.cache/doas.conf
        sudo cp ~/.cache/doas.conf /etc/doas.conf
        rm ~/.cache/doas.conf
        doas pacman -R sudo && doas ln -s /bin/doas /bin/sudo                            #TODO ARCH SPECIFIC
    fi



    sudo groupadd nogroup   #needed to use slock

    ### modify udevil config to add exec to mount options
    #sudo sed -i 's/^allowed_options .*$/&, exec/g' /etc/udevil/udevil.conf

    ### change dns to opendns for porn  blocking
    #sudo sh -c 'echo "static domain_name_servers=208.67.222.123 208.67.220.123" >> /etc/dhcpcd.conf'
    ### change dns to dns.watch, free speech dns
    #sudo sh -c 'echo "static domain_name_servers=84.200.69.80 84.200.70.40 2001:1608:10:25::1c04:b12f 2001:1608:10:25::9249:d69b" >> /etc/dhcpcd.conf'
}

[ "$USER" = "root" ] && echo "do not run as root/sudo" && exit

pkgm_install
setup

#sh ~/.local/installers/suckless-install.sh
