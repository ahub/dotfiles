#!/bin/sh
if [ "$(pgrep picom)" ];then
    pkill picom
else
    picom &
fi
