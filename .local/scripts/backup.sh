#!/usr/bin/env sh
BACKUP_LOC="/media/SEAGATE/HOME_BACKUP_2/"
#EXCLUDE_CONF="$HOME/.config/rsync-exclude.conf"

notify-send "backup.sh" "performing backup"
if [ -d $BACKUP_LOC ]; then 
	rsync -arP --delete  --exclude-from="$EXCLUDE_CONF" ~/ $BACKUP_LOC
else
	notify-send "backup.sh" "backup location not found"
fi
