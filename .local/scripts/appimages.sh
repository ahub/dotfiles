#!/usr/bin/env sh

AIDIR="$HOME/.local/appimages"

if [ $1 ]; then
	AIDIR="$1"
fi

sel=$(ls "$AIDIR" | dmenu -l 5 -p "select AppImage")

if [ $sel ]; then
	sh -c $AIDIR/$sel
fi

