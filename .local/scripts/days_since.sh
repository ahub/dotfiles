#!/bin/bash

SCOREFILE="$1".score

DAY_INC=1
UPDATE_DEC="-1"

score_update() {
    CHNG=""

    ! [ -f "$SCOREFILE" ] && echo "0" > "$SCOREFILE"
    
    ! [ "$(echo "$SCOREFILE" | grep ".score")" ] && echo "Not a score file, data will be erased! Exiting." && exit

    VAL="$(echo "$1" | sed 's/-//g')" #remove negative sign for number validation
    case $VAL in
        ''|*[!0-9]*) echo "need number input for score_update function" ;;
        *) CHNG="$1" ;;
    esac

    #load current score from scorefile
    VAR="$( awk '{print $1}' < "$SCOREFILE" )"

    VAR=$((VAR + CHNG))

    echo "$VAR" > "$SCOREFILE"
}

help() {
    echo "\$1 -> file to update or compare against"
    echo "\$2 -> -u -> update file given by \$1"
}

! [ -f "$1" ] && touch "$1" 

#Command for ~/.cache/last_fap
if [ "$2" = "-u" ]; then

    #decrement score for current update
    score_update  $UPDATE_DEC

    #calc number of days since last update and increment score by number of days without update 
    LAST="$( awk 'END{print $1}' < "$1" )"
    DAYS=$(( (($(date --date="$(date +%y%m%d)" +%s) - $(date --date="$LAST" +%s) ))/(60*60*24) ))
    score_update $((DAY_INC * DAYS )) 

    #TODO add check before overwriting this file
    echo "$(date "+%y%m%d %H")" >> "$1" #reset days since update 

else

    LAST="$( awk 'END{print $1}' < "$1" )"
    HOUR="$( awk 'END{print $2}' < "$1" )"".0"
    HOUR_NOW="$(date +%H)"".0"

    DAYS=$(( (($(date --date="$(date +%y%m%d)" +%s) - $(date --date="$LAST" +%s) ))/(60*60*24) ))

    RET=$(bc <<< "scale=2; $DAYS + (($HOUR_NOW - $HOUR)/24.0)")

    echo "Days since : $RET"
    
    if [ -f "$SCOREFILE" ]; then
        SCORE="$(cat $SCOREFILE)"
        echo "Current score is: $SCORE" 
    fi
fi
