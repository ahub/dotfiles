#!/bin/sh
#=========================================================================
# This script is used to set the status bar for dwm, or 
# other wms that use xsetroot for a status bar. 
#
# by default this script will only run once, to run in 
# a loop give it the -l flag: ./dwm-bar.sh -l
#
# Requires: pulsemixer, xbps (for sys_updates) 
# 
# ddate -> creates a date string
# audio -> creates a string representing the state of pulseaudio
# netup -> creates a string for the current network interface and status
# weather -> reads file written by ~/scripts/update-weather.sh to set the weather 
#
# update -> calls all the above functions into xsetroot to set status bar
#
# https://gitlab.com/ahub/dotfiles
#========================================================================

MAIL_DIR="$HOME/.local/share/mail"

SEP="  |  "

ddate () {
	printf " %s\n" "$(date "+%b %d, %Y ( %I:%M )")" #(%a)
}


audio () {
    #icon="$(pulsemixer --get-volume | cut -d ' ' -f 1) 🔊"
    #[ $(pulsemixer --get-mute) = "1" ] && icon="🔇"

   volstat="$(amixer get Master)"
    if [  -n "$(echo "$volstat" | grep "\[off\]")" ]; then
        icon="🔇"
    else
		icon="$(echo "$volstat" | grep -o "\[[0-9]\+%\]" | sed 's/[^0-9]*//g' | sed -n 1p -) 🔊"
    fi

    printf "%s\n" "$icon"
}

nofap_counter() {
    DAYS="$(zsh ~/.local/scripts/days_since.sh ~/.cache/last_fap)"
    printf "🍆💦💦 $DAYS"
}

rss() {
    rss_cnt=$(sfeed_plain ~/.local/share/sfeed/feeds/*  | grep -c "^N" )
    printf "%s \n" "$rss_cnt"
}

mail() {
    #MAIL_DIR="$HOME/.local/share/mail/main/INBOX/new"
    #COUNT="$(ls "$MAIL_DIR" | wc -l)"

    COUNT=0
    for MBOX in $(ls "$MAIL_DIR")
    do
        TMP_DIR="$MAIL_DIR""/$MBOX""/INBOX/new"
        COUNT=$((COUNT+$(ls "$TMP_DIR" | wc -l)))

    done


    icon="$COUNT 📧"

    printf "%s\n" "$icon"

}

netup() {
	icon="❗"
	for iface in $(ls -1 /sys/class/net | sed '/^lo/d')
	do
    	if [ $(cat /sys/class/net/"$iface"/operstate | grep up) ] ; then
        	if [ "$(echo "$iface" | grep w)" ]; then
            	icon=" "     
                # "📡"
        	else
            	icon=""   #"🌐"
        	fi
    	elif [ -z "$icon" ]; then
        	icon="❗"
    	fi
	done

	printf "%s\n" "$icon"
}

weather() {
	if ! [ -f ~/.cache/wttr ]; then
		sh ~/.local/scripts/update-weather.sh
	fi
	icon="$(cat ~/.cache/wttr)"

	printf "%s\n" "$icon"
}

#sysupdates() {
#	upcnt=$(xbps-install -un | wc -l)
#	icon=""
#
#	if [ $upcnt -eq 0 ]; then
#		icon=""
#	else
#		icon="$upcnt  "
#	fi
#
#	printf "%s\n" "$icon"
#}


cputemp() {
	icon="$(cat /sys/class/thermal/thermal_zone0/temp | sed 's/\(.\)..$/.\1°C/')"

	printf "%s\n" "$icon"
}

battery() {
	status="$(cat /sys/class/power_supply/BAT0/status)"
	charge="$(cat /sys/class/power_supply/BAT0/capacity)"
	icon=""
	if [ "$status" = "Discharging" ]; then
		icon="${charge}% 🔋"	
    elif [ -z "$status" ]; then
        icon="🔌"
	else 
		icon="${charge}% 🔌"	
	fi

	printf "%s\n" "$icon"
}

crypto() {
    icon="$(cat ~/.cache/rate)"
    printf "%s\n" "$icon"
}
#transmission() {
#}

update() {
    xsetroot -name " $(crypto)$SEP$(cputemp)$SEP$(battery)$SEP$(netup)$SEP$(audio)$SEP$(rss)$SEP$(weather)$SEP$(ddate) "
}


#trap 'exit' 2
#trap 'exit' 15
#trap 'exit' 9
if [ "$1" ] && [ "$1" = "-l" ]; then    
    echo "$$" > ~/.cache/statusbar_pid

    sh ~/.local/scripts/update-crypto.sh -i &
    sh ~/.local/scripts/update-weather.sh -i &

    while true
    do
        update    
        sleep 60 
    done
else 
    update
fi
