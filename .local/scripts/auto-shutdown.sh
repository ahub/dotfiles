#!/bin/sh
notify-send "auto-shutdown.sh" "System will power off in 30 minutes."
sleep 1500 
notify-send "auto-shutdown.sh" "System will power off in 5 minutes."
sleep 290
notify-send "auto-shutdown.sh" "Powering off now."
sleep 10
sudo poweroff
