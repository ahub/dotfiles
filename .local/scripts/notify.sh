#!/bin/sh

#run at startup in background to get notifications
# put it in .xinitrc as:
# sh -c 'tiramisu | notify.sh' &  
# or
# sh ~/.local/scripts/notify.sh &
#
# requires tiramisu and herbe to work.
# https://github.com/Sweets/tiramisu
# https://github.com/dudik/herbe
# 
# if input is piped into this script it will source that.
# if the script is run by itself, it will run tiramisu on its own


handle() {
    case "$line" in
        summary*) SUMMARY="$(echo "$line" | cut -f 2 -d ' ' )";; 
        body*) 
            BODY="$(echo "$line" | cut -f 2 -d ' ' )" 
            herbe "$SUMMARY" "$BODY"
            ;; 
    esac
}

#had issues running this from .xinitrc, putting in a 1 second sleep fixed it
sleep 1 

SUMMARY=""
BODY=""

# Check to see if a pipe exists on stdin.
if [ -p /dev/stdin ]; then
    while IFS= read -r line; do
        handle "$line"
    done
else
    tiramisu |
        while read -r line; do 
            handle "$line"
        done
fi
