#!/bin/sh

if [ "$#" = "2" ]; then
SSID="$1"
PASSWORD="$2"

sudo sh -c "wpa_passphrase $SSID $PASSWORD >> /etc/wpa_supplicant/wpa_supplicant.conf"

sudo sv dhcpcd restart
else 
    echo "$1 = wifi network name (SSID)"
    echo "$2 = wifi password"
fi


