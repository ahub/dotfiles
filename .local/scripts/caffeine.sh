#!/bin/sh

if [ "$1" = "on" ]; then
    notify-send "caffeine.sh" 'Screen timeout is disabled.'
    xset -dpms
    xset s off
    
    pkill xautolock
else
    notify-send "caffeine.sh" "Screen timeout is enabled."
    xset +dpms
    xset s on

    if [ -z "$(pgrep xautolock)" ]; then
        xautolock -time 30 -locker slock &
    fi
fi
