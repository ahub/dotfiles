#!/bin/sh

[ "$(pgrep pipewire)" ] && pipewire &
[ "$(pgrep pipewire-pulse)" ] && pipewire-pulse &
sleep 1
[ "$(pgrep pipewire-media-session)" ] && pipewire-media-session &
