#!/bin/sh
###SUCKLESS INSTALL###

INSTALL_DIR="/home/alex/.local/src"

clone() {
    cd "$INSTALL_DIR" || return
    if [ "$( echo "$1" | grep -v "^#")" ]; then
         git clone "$1"
    fi
}

add_repos() {
    mkdir -p $INSTALL_DIR

    if [ -p /dev/stdin ]; then
        while IFS= read -r line; do
            clone "$line"
        done
    else
        FN="$1"
        ! [ -f "$FN" ] && FN=
        cat "$1" |
            while read -r line; do 
                clone "$line"
            done
    fi

    do_all install
}

update() {
    cd "$INSTALL_DIR"/"$1" || return 
    git pull origin master
    #install "$1"
}

list() {
    ls -l "$INSTALL_DIR" | grep -v "total" | rev | cut -d ' ' -f 1 | rev
}

do_all() {
    for i in $(list)
    do
        $1 "$i"
    done
}

uninstall() {
    cd "$INSTALL_DIR"/"$1" || return 
    case "$1" in
        tiramisu*) 
            sudo rm /usr/local/bin/tiramisu
            ;;
        pfetch*) 
            sudo rm /usr/local/bin/pfetch 
            ;;
        yacy_search_server*)
            echo "ADD UNINSTALL FUNCTIONALITY FOR THIS REPO"
            ;;
        pipe-viewer* | straw-viewer*)
            sudo ./Build uninstall       #TODO test if this actually works
            ;;
        mcomix3)
            rm "$INSTALL_DIR"/bin/mcomix
            rm -rf "$INSTALL_DIR"/share/mcomix/
            rm /usr/share/applications/mcomix.desktop
            ;;
        *)
            sudo make uninstall
            ;;
    esac
}

install() {
    cd "$INSTALL_DIR"/"$1" || return 
    case "$1" in
        tiramisu*) 
            make
            sudo cp tiramisu /usr/local/bin/
            ;;
        pfetch*) 
            sudo cp pfetch /usr/local/bin/
            ;;
        yacy_search_server*)
            ant clean all dist
            ;;
        pipe-viewer* | straw-viewer*)
            perl Build.PL
            sudo ./Build installdeps
            sudo ./Build install
            ;;
        mcomix3)
            DEST_DIR="/usr/local"
            mkdir -p "$DEST_DIR"/bin
            mkdir -p "$DEST_DIR"/share

            sudo python3 installer.py --srcdir=mcomix --target="$DEST_DIR"/share
            sudo ln -s "$DEST_DIR"/share/mcomix/mcomixstarter.py "$DEST_DIR"/bin/mcomix
            sudo cp ./mime/mcomix.desktop /usr/share/applications/
            ;;

        tremc)
            sudo sh -c 'export DESTDIR="/usr/local" && make install'
            ;;
        *)
            sudo make install
            ;;
    esac
}

help() {
    echo "add-repos   -> add collection of git repos to dir, from list of repos in file specified by \$2"
    echo "add-repo    -> add one specific repo to be handled by this script specified by \$2" 
    echo "list        -> list all currently tracked repos"
    echo "upgrade     -> install all repos currently being tracked" 
    echo "install     -> install specified program, should be name of repo seen from output of list"
    echo "uninstall   -> uninstall specified program, should be name of repo seen from output of list"
    echo "update      -> pull all repos currently being tracked"
    echo "clean       -> runs uninstall command on all repos currently being tracked"
}


case "$1" in
    add-repos)   add_repos "$2";;  #add collection of git repos to dir, from list of repos in file specified by $2
    add-repo)    clone "$2";;      #add one specific repo to be handled by this script specified by $2 
    list*)       list;;            #list all currently tracked repos
    upgrade)     do_all install;;  #install all repos currently being tracked to the system 
    install*)    install "$2";;    #install specified program, should be name of repo seen from output of "list"
    uninstall*)  uninstall "$2";;  #uninstall specified program, should be name of repo seen from output of "list"
    update*)     do_all update;;
    clean*)      do_all uninstall;;           #runs uninstall command on all repos currently being tracked
    *)           help;;


esac
