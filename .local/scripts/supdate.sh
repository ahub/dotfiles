#!/bin/sh
#
# this script will pull updates from the suckless.org website and merge them into
# your local repository.
#
# suckless.org is assumed to be the upstream url, 
# if a different url is desired, modify the UPSTREAM_URL variable accordingly

# This script assumes all suckless repos are in the ~/.local/src/ directory.
# If your repos are in a different location, update the REPO_LOC variable accordingly
#
# These variables can either be modified in the script below directly, 
# or defined before calling this script, ex:
#
#   export REPO_LOC="https://github.com" 
#   sh supdate.sh -a dwm
#
# ARGUMENTS USED:
# $1 -> -p = pull updates, -m = merge updates, -a = add upstream branch 
# $2 -> name of program to update, one of: dwm, st, dmenu, surf 
#

# HOW TO USE:
#
# dwm is used as an example here, replace "dwm" with any other suckless program name as needed.
# Make sure the name is as it appears in the git url.
#
# On the first time using this script, add a branch to hold the upstream repo for this repository
#
#   sh supdate.sh -a dwm
#
# You should first pull any updates to the repository with:
#
#   sh supdate.sh -p dwm
#
# That will create a temp branch with master merged with upstream updates.
# Fix any merge conflicts and commit to the temporary updates branch created above.
# Then run merge those updates to master with:
# 
#   sh supdate.sh -m dwm
#
##########################################################################################

[ -z "$REPO_LOC" ]     && REPO_LOC="$HOME/.local/src"
[ -z "$UPSTREAM_URL" ] && UPSTREAM_URL="https://git.suckless.org"

pull_updates() {

    if ! [ "$1" = "" ]; then 

        cd "$REPO_LOC"/"$1" || return

        git checkout upstream
        git remote add upstream "$UPSTREAM_URL"/"$1"
        git pull upstream master:upstream
        git checkout -b updates
        git merge master

        echo "Fix any merge errors here and commit them to branch \"updates\" "
        echo "Once that is done, run this script with the -m flag to merge these updates to master."
    else
        echo "provide name of the program to update, one of: dwm, st, dmenu, surf"
    fi 
}

merge_updates() {

    if ! [ "$1" = "" ]; then 

        cd "$REPO_LOC"/"$1" || return

        git checkout master
        git merge updates
        git branch -d updates

        sudo make clean install
    fi
}

add_upstream_branch() {
    if ! [ "$1" = "" ]; then 

        cd "$REPO_LOC"/"$1" || return

        git checkout -b upstream
        git remote add upstream "$UPSTREAM_URL"/"$1"
        git fetch --all
        git reset --hard upstream/master
    fi
}

help() {
    echo "\$1 -> -p = pull updates, -m = merge updates"
    echo "\$2 -> name of program to update, one of: dwm, st, dmenu, surf"
}

if [ "$#" = "2" ]; then 
    case "$1" in
        -p) pull_updates "$2";;
        -m) merge_updates "$2";;
        -a) add_upstream_branch "$2";;
        *) help;; 
    esac
else 
    help
fi
