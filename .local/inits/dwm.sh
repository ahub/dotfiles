#!/bin/sh
xrdb ~/.config/Xresources

#put any device specific initialization in ~/init.sh
[ -f "$HOME"/.init.sh ] && sh "$HOME"/.init.sh

#[ "$(pgrep pipewire)" ] && /usr/bin/pipewire &
#[ "$(pgrep pipewire-pulse)" ] && /usr/bin/pipewire-pulse &
#sleep 1
#[ "$(pgrep pipewire-media-session)" ] && /usr/bin/pipewire-media-session &

#gentoo wiki recommended this for logind
exec dbus-launch --exit-with-session dwm
