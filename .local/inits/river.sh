#!/bin/sh

SERVICES="
waybar
pipewire
pipewire-pulse
pipewire-media-session
wlsunset -t 3000 -l 34.0 -L -86.0
devmon
mpd
mako
"

echo "$SERVICES" | while read -r i
do

    if [ "$i" = "pipewire-media-session" ]; then
        [ -z "$(pgrep "pipewire-media" )" ] &&  "$i" &
    else
        ! [ "$(pgrep "$i" )" ] && $i  2> /dev/null 1> /dev/null & 
    fi

    
done 

swaybg --image ~/.config/wall &

swayidle -w \
                timeout 300 'swaylock -f -c 000000' \
                timeout 600 'swaymsg "output * dpms off"' \
                     resume 'swaymsg "output * dpms on"' \
                before-sleep 'swaylock -f -c 000000' &

# ! [ "$( pgrep sxhkd)" ] && sxhkd &

#wait
