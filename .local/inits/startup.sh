#!/usr/bin/env sh

_shutdown() {
    pkill sxhkd 
    pkill tiramisu 
    pkill picom 
    pkill redshift 
    pkill mpd 
    pkill devmon 
    kill sleep -P "$(cat ~/.cache/statusbar_pid )"
    kill "$(cat ~/.cache/statusbar_pid )"
}

startup() {
    [ "$(pgrep redshift)" ] && _shutdown

    xrdb ~/.config/Xresources
    sxhkd &
    #dunst &
    sh ~/.local/scripts/notify.sh &
    #picom --backend glx --glx-fshader-win "$(cat ~/.config/picom/grayscale.glsl)"  &   #make entire display grayscale, comment out picom below if using
    picom &
    redshift &
    mpd &
    #xautolock -time 30 -locker slock &
    xwallpaper --zoom ~/.config/wall &
    devmon &
    #[ -z "$(pgrep transmission)" ] && sh ~/.local/scripts/trans-init &
    #[ -z "(pgrep scron)" ] && scrond -f ~/.config/crontab &
}

