#!/bin/sh

xrdb ~/.config/Xresources

#put any device specific initialization in ~/.init.sh
[ -f "$HOME"/.init.sh ] && sh "$HOME"/.init.sh

exec dbus-launch --exit-with-session spectrwm 
